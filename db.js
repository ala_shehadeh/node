const mysql = require('mysql');
const database = require('./config')

// connection configurations
const mc = mysql.createConnection(database.database);

// connect to database
mc.connect();

exports.mc = mc