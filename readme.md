###Node Articles CRUD system

this is simple CRUD (Create, Read, Update, Delete) system built by Node JS

to run the application you have to have the following softwares at your PC/Server

- Node server
- mysql
- postmanapp (import the node.json to test the service)

to implement the application do the following:

- on your command prompt run the following command to install the dependencies  
/node-path/npm install
- open config.js and insert your mysql information and credentials
- run the following command to install the table 
/node-path/node dbcreate
- run the following command to lunch the system
/node-path/npm start