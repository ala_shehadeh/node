mc = require('../db')


controller = {
    allArticles: function (req, res) {
        mc.mc.query('SELECT * FROM articles', function (error, results) {
            if (error) throw error;
            if(results.length == 0)
                res.status(404).send({error: 'No articles added'});
            else
                return res.send({ data: results});
        });
    },
    addArticle: function (req, res) {
        let errors = this.articleValidation(req)
        if(errors.length > 0)
            return res.status(404).send({error:errors})
        else {
            mc.mc.query('insert into articles set ?',{body:req.body.body,title:req.body.title},function (error) {
                if (error) throw error;
                return res.send({success:true})
            })
        }
    },
    articleValidation: function (data) {
        let errors = new Array();
        if(!data.body) {
            errors.push('No body request')
            return errors;
        }
        let body = data.body.body;
        let title = data.body.title;

        //check input data
        if(body == '' || !body)
            errors.push('body text is required')
        if(title == '' || !title)
            errors.push('title is required')
        return errors;
    },
    selectedArticle: function (req,res) {
        let article_id = req.params.id;

        mc.mc.query('SELECT * FROM articles where id=?', article_id, function (error, results, fields) {
            if (error) throw error;
            if(results.length == 0)
                return res.status(404).send({error: "the selected article not exist"});
            else
                return res.send(results[0]);
        });

    },
    deleteArticle: function (req,res) {
        var article_id = req.params.id;
        if (!article_id)
            return res.status(404).send({ error: 'Please select the article'});
        else {
            var $this = this;
            mc.mc.query('DELETE FROM articles WHERE id = ?', [article_id], function (error) {
                if (error) throw error;
                return res.send({success:true});
            });
        }
    },
    updateArtice: function (req,res) {
        let article_id = req.body.id
        let errors = this.articleValidation(req)
        if(errors.length > 0)
            return res.status(404).send({error:errors})
        else {
            mc.mc.query('update articles set title=?,body=? where id=?',[req.body.title,req.body.title,article_id],function (error) {
                if (error) throw error;
                return res.send({success:true})
            })
        }
    }
}

    exports.CRUD = controller