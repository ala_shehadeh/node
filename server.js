const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const controller = require('./controllers/articlesController')

// port must be set to 8080 because incoming http requests are routed from port 80 to port 8080
app.listen(8080, function () {
    console.log('Node app is running on port 8080');
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.get('/articles', function (req,res,next) {
    return controller.CRUD.allArticles(req,res);
});

app.post('/article', function (req,res,next) {
    return controller.CRUD.addArticle(req,res);
});

app.get('/article/:id',function (req,res,next) {
    return controller.CRUD.selectedArticle(req,res);
});

app.put('/article',function (req,res,next) {
    return controller.CRUD.updateArtice(req,res);
})

app.delete('/article/:id',function (req,res,next) {
    return controller.CRUD.deleteArticle(req,res);
})

